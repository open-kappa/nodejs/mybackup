# @open-kappa/mybackup
This package provides a simple utility program to perform backups.

# Links

 * Project homepage: [hosted on GitLab Pages](
   https://open-kappa.gitlab.io/nodejs/mybackup)

 * Project sources: [hosted on gitlab.com](
   https://gitlab.com/open-kappa/nodejs/mybackup)

 * List of open-kappa projects, [hosted on GitLab Pages](
   https://open-kappa.gitlab.io)

# License

*@open-kappa/mybackup* is released under the liberal MIT License.
Please refer to the LICENSE.txt project file for further details.

# Patrons

This node module has been sponsored by [Gizero Energie s.r.l.](
www.gizeroenergie.it).
