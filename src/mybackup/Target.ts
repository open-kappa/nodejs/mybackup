import {
    Audit,
    BackupCommand,
    checkExists,
    JsonDestination,
    JsonTarget,
    makeCollectStdoutCommand,
    makeExecCommand,
    makeSpawnCommand,
    RetentionManager,
    TargetResult,
    Task,
    TaskResult
} from "./mybackupImpl";
import {
    foreachPromise,
    toVoid
} from "@open-kappa/mypromise";
import {
    IMyCommandConfig,
    IMyFileConfig,
    MyCommandLogger,
    MyFileLogger
} from "@open-kappa/mylog";
import {DateTime} from "luxon";
import {mergeJson} from "@open-kappa/myjson";
import path from "path";


export class Target
{
    private readonly name: string;
    readonly command: BackupCommand;
    private isTopLevel: boolean;
    private isDisabled: boolean;
    private config: JsonTarget;
    private parentConfig: JsonTarget;
    private readonly tasks: Array<string>
    private hasSubTargets: boolean;
    private parentTarget: Target|null;
    private readonly results: Array<TargetResult>;
    private hasAudit: boolean;
    private thisResult: TargetResult;
    destination: JsonDestination|null;
    private startTimestamp: string;
    private endTimestamp: string;

    /**
     * @brief Constructor.
     * @param {string} name This target name.
     * @param {BackupCommand} command The command.
     * @param {Target|null} parentTarget The parent target.
     */
    constructor(
        name: string,
        command: BackupCommand,
        parentTarget: Target|null
    )
    {
        this.name = name;
        this.command = command;
        this.isTopLevel = true;
        this.isDisabled = false;
        this.config = {};
        this.parentConfig = {};
        this.tasks = [];
        this.hasSubTargets = false;
        this.parentTarget = parentTarget;
        this.results = [];
        this.hasAudit = false;
        this.thisResult = {
            "gitResult": TaskResult.SUCCESS,
            "hasGit": false,
            "hasTgz": false,
            "names": [],
            "results": [],
            "targetName": this.getName(),
            "tgzResult": TaskResult.SUCCESS
        };
        this.destination = null;
        this.startTimestamp = "";
        this.endTimestamp = "";
    }

    /**
     * @brief Get the name.
     * @return {string} The name.
     */
    getName(): string
    {
        const self = this;
        if (self.parentTarget === null) return self.name;
        return self.parentTarget.getName() + "/" + self.name;
    }

    /**
     * @brief Get the start timestamp.
     * @return {string} The start timestamp.
     */
    getStartTimestamp(): string
    {
        return this.startTimestamp;
    }

    /**
     * @brief Get the end timestamp.
     * @return {string} The end timestamp.
     */
    getEndTimestamp(): string
    {
        return this.endTimestamp;
    }

    /**
     * @brief Run this target.
     * @param {JsonTarget|null} parentConfig The parent config.
     * @return {Promise<void>} A completion promise.
     */
    run(
        parentConfig: JsonTarget|null
    ): Promise<void>
    {
        const self = this;
        self.command.main.log.info(self.makeLog("Started execution."));
        self.isTopLevel = parentConfig === null;

        function configAudit(): Promise<void>
        {
            return self.configAudit();
        }
        function configDestination(): Promise<void>
        {
            return self.configDestination();
        }
        function runSubTargets(): Promise<void>
        {
            return self.runSubTargets();
        }
        function runTasks(): Promise<void>
        {
            return self.runTasks();
        }
        function addResults(): Promise<void>
        {
            return self.addResults();
        }
        function manageGit(): Promise<void>
        {
            return self._manageGit();
        }
        function manageTgz(): Promise<void>
        {
            return self._manageTgz();
        }
        function notifyAlarms(): Promise<void>
        {
            return self._notifyAlarms();
        }
        // ////////////////////////

        function manageConfig(): Promise<void>
        {
            return self.manageConfig(parentConfig);
        }
        function prepareTasks(): Promise<void>
        {
            self.command.main.log.log(self.makeLog("Configuring tasks..."));
            return self.prepareTasks();
        }
        function isEnabled(): Promise<void>
        {
            if (self.isDisabled)
            {
                return Promise.resolve();
            }
            else if (!self.hasSubTargets && self.tasks.length === 0)
            {
                self.command.main.log.warn(self.makeWarn("No actions found."));
                return Promise.resolve();
            }
            return configAudit()
                .then(configDestination)
                .then(runSubTargets)
                .then(runTasks)
                .then(manageGit)
                .then(manageTgz)
                .then(addResults)
                .then(notifyAlarms);
        }
        function logCompletion(): Promise<void>
        {
            self.command.main.log.info(self.makeLog("Completed."));
            return Promise.resolve();
        }

        return manageConfig()
            .then(prepareTasks)
            .then(isEnabled)
            .then(logCompletion);
    }

    /**
     * @brie Manage the configuration.
     * @param {JsonTarget|null} parentConfig The parent config.
     * @return {Promise<void>} A completion promise.
     */
    private manageConfig(
        parentConfig: JsonTarget|null
    ): Promise<void>
    {
        const self = this;

        const thisConfig = self.command.main.config.json[self.name];
        const invalidNames = ["version"];
        if (typeof thisConfig === "undefined")
        {
            return Promise.reject(
                self.makeError("Missing target config: " + self.name)
            );
        }
        else if (invalidNames.indexOf(self.name) !== -1)
        {
            return Promise.reject(
                self.makeError("Invalid target name: " + self.name)
            );
        }
        self.command.main.log.log(self.makeLog("Configuring..."));
        self.config = thisConfig;
        if (parentConfig !== null) self.parentConfig = parentConfig;

        self.isDisabled = typeof thisConfig.disabled !== "undefined"
            && thisConfig.disabled;
        if (self.isDisabled) return Promise.resolve();

        const parentHasDestination =
            parentConfig !== null
            && typeof parentConfig.destination !== "undefined";
        const hasAudit = typeof thisConfig.audit !== "undefined";
        const hasDestination = typeof thisConfig.destination !== "undefined";
        const hasSubTargetsDef = typeof thisConfig.subTargets !== "undefined";
        const hasSubTargets = typeof thisConfig.subTargets !== "undefined"
            && (thisConfig.subTargets.length > 0);
        const hasDisabled = typeof thisConfig.disabled !== "undefined";
        const hasTasks = Object.keys(thisConfig).length
            - (hasAudit ? 1 : 0)
            - (hasDestination ? 1 : 0)
            - (hasSubTargetsDef ? 1 : 0)
            - (hasDisabled ? 1 : 0)
            > 0;
        self.hasSubTargets = hasSubTargets;
        self.hasAudit = hasAudit;
        if (hasAudit)
        {
            self.startTimestamp = DateTime.fromJSDate(new Date())
                .toFormat("yyyy-MM-dd HH:mm:ss");
        }
        else if (self.parentTarget !== null)
        {
            self.startTimestamp = self.parentTarget.startTimestamp;
        }
        else
        {
            return Promise.reject(
                self.makeError("Internal error: missing parent target")
            );
        }

        if (hasDestination)
        {
            self.destination = thisConfig.destination as JsonDestination;
            if (self.destination.git?.enabled)
            {
                self.thisResult.hasGit = true;
            }
            if (self.destination.tgz?.enabled)
            {
                self.thisResult.hasTgz = true;
            }
        }
        else if (parentHasDestination)
        {
            self.destination = parentConfig?.destination as JsonDestination;
        }

        if (!self.isTopLevel)
        {
            if (hasAudit)
            {
                return Promise.reject(
                    self.makeError("Audit can appear only in top-level target")
                );
            }
            if (hasDestination && !parentHasDestination && hasTasks)
            {
                return Promise.reject(
                    self.makeError("Missing destination")
                );
            }
        }
        else
        {
            if (!hasAudit && hasTasks)
            {
                self.command.main.log.warn(
                    self.makeWarn("No audit configured")
                );
            }
            if (!hasDestination && hasTasks)
            {
                return Promise.reject(
                    self.makeError("Missing destination configuration.")
                );
            }
        }

        return Promise.resolve();
    }

    /**
     * @brie prepare the tasks.
     * @return {Promise<void>} A completion promise.
     */
    private prepareTasks(): Promise<void>
    {
        const self = this;
        const nonTasks = [
            "audit",
            "destination",
            "subTargets",
            "disabled"
        ];
        function filter(value: string): boolean
        {
            return nonTasks.indexOf(value) === -1;
        }
        const tasks = Object.keys(self.config).filter(filter);
        Array.prototype.push.apply(self.tasks, tasks);
        return Promise.resolve();
    }

    /**
     * @brief Configures and validates the audit.
     * @private
     * @return {Promise<void>} A completion promise.
     */
    private configAudit(): Promise<void>
    {
        const self = this;
        const tmpAudit = self.config.audit;
        if (typeof tmpAudit === "undefined")
        {
            return Promise.resolve();
        }
        self.command.main.log.log(self.makeLog("Configuring auditing..."));

        const audit = tmpAudit;

        function makeLogDir(logDir: string): Promise<void>
        {
            const spawnMake = makeSpawnCommand(
                "",
                "",
                self.command.main.log
            );
            const destinationCmd = [
                "mkdir",
                "-p",
                logDir
            ];
            return spawnMake(destinationCmd);
        }

        function addFileLogger(): Promise<void>
        {
            const config: IMyFileConfig = {
                "append": false,
                "outputFile": audit.logFile
            };
            const logger = new MyFileLogger(config);
            self.command.main.log.addLogger(logger);
            return Promise.resolve();
        }

        function manageLogFile(): Promise<void>
        {
            if (audit.logFile === "") return Promise.resolve();
            const logDir = path.dirname(audit.logFile);
            return makeLogDir(logDir)
                .then(addFileLogger);
        }

        function manageScripts(): Promise<void>
        {
            if (audit.scripts.length === 0) return Promise.resolve();
            const config: IMyCommandConfig = {
                "commands": audit.scripts
            };
            const logger = new MyCommandLogger(config);
            self.command.main.log.addLogger(logger);
            return Promise.resolve();
        }

        function setVerbose(): Promise<void>
        {
            const verbose = self.command.main.cli.getVerboseLevel();
            if (verbose !== null) self.command.main.log.setVerbosity(verbose);
            return Promise.resolve();
        }

        return manageLogFile()
            .then(manageScripts)
            .then(setVerbose);
    }

    /**
     * @brief Configures and validates the destination.
     * @private
     * @return {Promise<void>} A completion promise.
     */
    private configDestination(): Promise<void>
    {
        const self = this;
        const tmpDst = self.config.destination;
        if (typeof tmpDst === "undefined")
        {
            return Promise.resolve();
        }
        self.command.main.log.log(
            self.makeLog("Configuring destination...")
        );

        const destination = tmpDst;

        function validateDestination(): Promise<void>
        {
            const hasGit = typeof destination.git !== "undefined"
                && destination.git.enabled;
            const hasTgz = typeof destination.tgz !== "undefined"
                && destination.tgz.enabled;
            if (hasGit && hasTgz)
            {
                return Promise.reject(
                    self.makeError(
                        "In the destination just one of GIT or TGZ"
                        + " must be enabled."
                    )
                );
            }
            else if (!hasGit && !hasTgz)
            {
                self.command.main.log.warn(
                    self.makeWarn(
                        "Destination is neither GIT nor TGZ."
                    )
                );
            }
            return Promise.resolve();
        }

        function checkDestinationExists(): Promise<boolean>
        {
            if (destination.tgz?.enabled)
            {
                const outDir = self.getStartTimestamp().split(" ")[0];
                destination.path = path.join(destination.path, outDir);
            }
            const checkFoo = checkExists(
                destination.user,
                destination.host,
                self.command.main.log
            );
            return checkFoo(destination.path);
        }

        return validateDestination()
            .then(checkDestinationExists)
            .then(toVoid);
    }

    /**
     * @brief Run the sub-targets.
     * @private
     * @return {Promise<void>} A completion promise.
     */
    private runSubTargets(): Promise<void>
    {
        const self = this;
        const subs = self.config.subTargets;
        if (typeof subs === "undefined" || subs.length === 0)
        {
            return Promise.resolve();
        }
        self.command.main.log.log(self.makeLog("Running sub-targets..."));

        function execSubTarget(name: string): Promise<void>
        {
            const subTgt = new Target(
                name,
                self.command,
                self
            );
            function run(cfg: any): Promise<void>
            {
                return subTgt.run(cfg);
            }
            const c1 = JSON.parse(JSON.stringify(self.config));
            const c2 = JSON.parse(JSON.stringify(self.parentConfig));
            return mergeJson([c1, c2])
                .then(run);
        }

        const all = foreachPromise(execSubTarget);
        return all(subs)
            .then(toVoid);
    }

    /**
     * @brief Run the tasks.
     * @private
     * @return {Promise<void>} A completion promise.
     */
    private runTasks(): Promise<void>
    {
        const self = this;
        if (self.tasks.length === 0) return Promise.resolve();
        self.command.main.log.log(self.makeLog("Running tasks..."));
        function runSingleTask(task: string): Promise<TaskResult>
        {
            const taskConfig = self.command.main.config
                .json[self.name][task];
            const taskObj = new Task(
                task,
                self,
                taskConfig
            );
            return taskObj.run();
        }
        function collectResults(res: Array<TaskResult>): Promise<void>
        {
            return self.prepareResults(res);
        }

        const foreachTask = foreachPromise(runSingleTask);
        return foreachTask(self.tasks)
            .then(collectResults);
    }

    /**
     * @brief Push the results to the nearest parent with audit.
     * @param {Array<TaskResult>} res The results.
     * @return {Promise<void>} A completion promise.
     */
    private prepareResults(res: Array<TaskResult>): Promise<void>
    {
        const self = this;
        self.thisResult.names = self.tasks;
        self.thisResult.results = res;
        return Promise.resolve();
    }

    /**
     * @brief Manage error condition during destination management.
     * @private
     * @param {Error} err The error.
     * @param {boolean} isGit true if it is a GIT management error.
     * @return {Promise<void>} A completion promise.
     */
    private _onDestinationmanagementError(
        err: Error,
        isGit: boolean
    ): Promise<void>
    {
        const self = this;
        const log = self.command.main.log;
        if (isGit)
        {
            log.error(self.makeErrorString("Error during GIT management"));
            self.thisResult.gitResult = TaskResult.ERROR;
        }
        else
        {
            log.error(self.makeErrorString("Error during TGZ management"));
            self.thisResult.tgzResult = TaskResult.ERROR;
        }
        log.error(err.message);
        return Promise.resolve();
    }

    /**
     * @brief Manage a GIT destination.
     * @private
     * @return {Promise<void>} A completion promise.
     */
    private _manageGit(): Promise<void>
    {
        const self = this;
        if (typeof self.config.destination === "undefined")
        {
            return Promise.resolve();
        }
        const destination = self.config.destination;
        if (typeof destination.git === "undefined"
            || !destination.git.enabled)
        {
            return Promise.resolve();
        }
        self.command.main.log.log(self.makeLog("Managing GIT..."));
        const log = self.command.main.log;
        function onError(err: Error): Promise<void>
        {
            return self._onDestinationmanagementError(err, true);
        }
        const cmd1 =
            "cd " + destination.path
            + " && git add .";
        // This can fail if no file must be committed:
        const cmd2 =
            "cd " + destination.path
            + " && git commit -m\"Backup " + self.getStartTimestamp() + "\"";
        const cmd3 =
            destination.git.push
                ? "cd " + destination.path + " && git pull && git push"
                : "";
        const cmdFoo1 = makeExecCommand(
            destination.user,
            destination.host,
            log
        );
        const cmdFoo2 = makeExecCommand(
            destination.user,
            destination.host,
            log
        );
        const cmdFoo3 = makeExecCommand(
            destination.user,
            destination.host,
            log
        );
        function doFoo1(): Promise<void>
        {
            return cmdFoo1([cmd1]);
        }
        function doFoo2(): Promise<void>
        {
            function suppressError(): Promise<void>
            {
                log.warn(self.makeWarn("Nothing to commit."));
                return Promise.resolve();
            }
            return cmdFoo2([cmd2])
                .catch(suppressError);
        }
        function doFoo3(): Promise<void>
        {
            if (cmd3 === "") return Promise.resolve();
            return cmdFoo3([cmd3]);
        }
        return doFoo1()
            .then(doFoo2)
            .then(doFoo3)
            .catch(onError);
    }

    /**
     * @brief Manage a TGZ destination.
     * @private
     * @return {Promise<void>} A completion promise.
     */
    private _manageTgz(): Promise<void>
    {
        const self = this;
        if (typeof self.config.destination === "undefined")
        {
            return Promise.resolve();
        }
        const destination = self.config.destination;
        if (typeof destination.tgz === "undefined"
            || !destination.tgz.enabled)
        {
            return Promise.resolve();
        }
        self.command.main.log.log(self.makeLog("Managing TGZ..."));
        const log = self.command.main.log;
        function onError(err: Error): Promise<void>
        {
            return self._onDestinationmanagementError(err, false);
        }
        const outDir = self.getStartTimestamp().split(" ")[0];
        const backupDir = path.join(destination.path, "..");
        function doTgz(): Promise<void>
        {
            log.log("Creating .tgz file...");
            const cmd =
                "cd " + backupDir
                + " && tar czf " + outDir + ".tgz " + outDir
                + " && rm -fr " + outDir;
            const cmdFoo = makeExecCommand(
                destination.user,
                destination.host,
                log
            );
            return cmdFoo([cmd]);
        }
        function doLs(): Promise<string>
        {
            const lsCmd = "ls " + path.join(backupDir, "*.tgz");
            const lsCmdFoo = makeCollectStdoutCommand(
                destination.user,
                destination.host,
                log,
                true
            );
            return lsCmdFoo([lsCmd]);
        }
        function manageRetention(lsOut: string): Promise<void>
        {
            log.log("Managing retention...");
            const manager = new RetentionManager(
                self.command.main,
                destination,
                backupDir
            );
            return manager.run(lsOut);
        }
        return doTgz()
            .then(doLs)
            .then(manageRetention)
            .catch(onError);
    }


    /**
     * @brief Add the children target results.
     * @param {TargetResult} res The results.
     */
    private addResults(): Promise<void>
    {
        const self = this;
        self.command.main.log.log(self.makeLog("Storing results..."));
        self.results.push(self.thisResult);
        if (!self.hasAudit && self.parentTarget !== null)
        {
            Array.prototype.push.apply(
                self.parentTarget.results,
                self.results
            );
        }
        return Promise.resolve();
    }

    /**
     * @brief Notify alarms.
     * @private
     * @return {Promise<void>} A completion promise.
     */
    private _notifyAlarms(): Promise<void>
    {
        const self = this;
        self.endTimestamp = DateTime.fromJSDate(new Date())
            .toFormat("yyyy-MM-dd HH:mm:ss");
        const audit = new Audit(
            self.command,
            self,
            self.config
        );
        return audit.run(self.results);
    }

    private makeError(msg: string): Error
    {
        const self = this;
        return new Error(self.makeErrorString(msg));
    }

    private makeErrorString(msg: string): string
    {
        const self = this;
        return self.getName() + ": [ERROR] " + msg;
    }

    private makeWarn(msg: string): string
    {
        const self = this;
        self.command.setHasWarnings();
        return self.getName() + ": [WARN] " + msg;
    }

    private makeLog(msg: string): string
    {
        const self = this;
        return self.getName() + ": " + msg;
    }
}
