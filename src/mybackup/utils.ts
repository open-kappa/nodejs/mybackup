import {
    collectStdout,
    exec,
    ExecOptions,
    spawn
} from "@open-kappa/mypromise";
import type {
    MyLogger
} from "@open-kappa/mylog";


/**
 * @brief Prepare the log message.
 * @private
 * @param {string} data The message.
 * @return {string} The prepared messsage.
 */
function _prepareMessage(data: string): string
{
    let msg = data;
    if (msg.endsWith("\n")) msg = msg.substring(0, msg.length - 1);
    if (msg.endsWith("\r")) msg = msg.substring(0, msg.length - 1);
    if (msg.endsWith("\f")) msg = msg.substring(0, msg.length - 1);
    return msg;
}

/**
 * @brief Do error log.
 * @private
 * @param {MyLogger|null} log The logger.
 * @param {string} data The messaage.
 */
function _logStderr(log: MyLogger|null, data: string): void
{
    if (log === null) return;
    const msg = _prepareMessage(data);
    if (msg === "") return;
    log.verboseError(msg);
}


/**
 * @brief Do stdout log.
 * @private
 * @param {MyLogger|null} log The logger.
 * @param {string} data The messaage.
 */
function _logStdout(log: MyLogger|null, data: string): void
{
    if (log === null) return;
    const msg = _prepareMessage(data);
    if (msg === "") return;
    log.verboseLog(msg);
}


/**
 * @brief Escape a command.
 * @param {string} cmd The command.
 * @return {string} The escaped command.
 */
function escapeCommand(cmd: string): string
{
    return "'"
        + cmd.split("'").join("'\"'\"'")
        + "'";
}


/**
 * @brief Check whether the path is remote.
 * @param {string} host The host name, or "localhost".
 * @return {boolean} The check result.
 */
function isRemoteHost(host: string): boolean
{
    return host !== ""
        && host !== "localhost";
}


/**
 * @brief Make the host for ssh.
 * @param {string} host The ssh host, or "localhost".
 * @param {string} path The inner path.
 */
function makeHost(user: string, host: string): string
{
    if (!isRemoteHost(host)) return "";
    const concatUser = user === ""
        ? ""
        : user + "@";
    return concatUser + host;
}


/**
 * @brief Create a path, possibly suitable for scp.
 * @param {string} user The ssh user name.
 * @param {string} host The ssh host.
 * @param {string} path The inner path.
 * @return {string} The resulting path.
 */
function makePath(user: string, host: string, path: string): string
{
    if (!isRemoteHost(host)) return path;
    const concatUser = user === ""
        ? ""
        : user + "@";
    return concatUser + host + ":" + path;
}


/**
 * @brief Create a spawn command.
 * @param {string} user The ssh user name.
 * @param {string} host The ssh host.
 * @param {MyLogger|null} log The logger.
 * @return {(cmd: Array<string>) => Promise<void>} The spawn function wrapper.
 */
function makeSpawnCommand(
    user: string,
    host: string,
    log: MyLogger|null
): (cmd: Array<string>) => Promise<void>
{
    function logStderr(data: string): void
    {
        _logStderr(log, data);
    }
    function logStdout(data: string): void
    {
        _logStdout(log, data);
    }
    const options: ExecOptions = {
        "checkCode": true,
        "logStderr": logStderr,
        "logStdout": logStdout
    };
    const executor = spawn(options);
    if (!isRemoteHost(host))
    {
        return executor;
    }
    const remoteHost = makeHost(user, host);
    function returnedExecutor(cmd: Array<string>): Promise<void>
    {
        const packed = escapeCommand(cmd.join(" && "));
        const wrappedCommand = [
            "ssh",
            remoteHost,
            packed
        ];
        return executor(wrappedCommand);
    }
    return returnedExecutor;
}


/**
 * @brief Create a spawn command.
 * @param {string} user The ssh user name.
 * @param {string} host The ssh host.
 * @param {MyLogger|null} log The logger.
 * @return {(cmd: Array<string>) => Promise<void>} The exec function wrapper.
 */
function makeExecCommand(
    user: string,
    host: string,
    log: MyLogger|null
): (cmd: Array<string>) => Promise<void>
{
    function logStderr(data: string): void
    {
        _logStderr(log, data);
    }
    function logStdout(data: string): void
    {
        _logStdout(log, data);
    }
    const options: ExecOptions = {
        "checkCode": true,
        "logStderr": logStderr,
        "logStdout": logStdout
    };
    const executor = exec(options);
    if (!isRemoteHost(host))
    {
        return executor;
    }
    const remoteHost = makeHost(user, host);
    function returnedExecutor(cmd: Array<string>): Promise<void>
    {
        const packed = escapeCommand(cmd.join(" && "));
        const wrappedCommand = [
            "ssh",
            remoteHost,
            packed
        ];
        return executor(wrappedCommand);
    }
    return returnedExecutor;
}


/**
 * @brief Wraps an exec or spawn, to get the command stdout as result.
 * @param {string} user The ssh user name.
 * @param {string} host The ssh host.
 * @param {MyLogger|null} log The logger.
 * @param {boolean} useExec Whether to use exec or spawn.
 * @return {(cmd: Array<string>) => Promise<string>} The function wrapper.
 */
function makeCollectStdoutCommand(
    user: string,
    host: string,
    log: MyLogger|null,
    useExec: boolean
): (cmd: Array<string>) => Promise<string>
{
    const executor = useExec ? exec : spawn;
    function logStderr(data: string): void
    {
        _logStderr(log, data);
    }
    function logStdout(data: string): void
    {
        _logStdout(log, data);
    }
    const options: ExecOptions = {
        "checkCode": true,
        "logStderr": logStderr,
        "logStdout": logStdout
    };
    const collector = collectStdout(executor, options);
    if (!isRemoteHost(host))
    {
        return collector;
    }
    const remoteHost = makeHost(user, host);
    function returnedCollector(cmd: Array<string>): Promise<string>
    {
        const packed = escapeCommand(cmd.join(" && "));
        const wrappedCommand = [
            "ssh",
            remoteHost,
            packed
        ];
        return collector(wrappedCommand);
    }
    return returnedCollector;
}


/**
 * @brief Copy from source to target recursively.
 * @param {string} srcUser The ssh source user name.
 * @param {string} srcHost The ssh source host.
 * @param {string} srcPath The ssh source path.
 * @param {string} dstUser The ssh destination user name.
 * @param {string} dstHost The ssh destination host.
 * @param {string} dstPath The ssh destination path.
 * @param {MyLogger|null} log The logger.
 * @return {() => Promise<void>} The copy function wrapper.
 */
function makeCopy(
    srcUser: string,
    srcHost: string,
    srcPath: string,
    dstUser: string,
    dstHost: string,
    dstPath: string,
    log: MyLogger|null
): () => Promise<void>
{
    const srcFullPath = makePath(srcUser, srcHost, srcPath);
    const dstFullPath = makePath(dstUser, dstHost, dstPath);
    const isRemoteSrc = isRemoteHost(srcHost);
    const isRemoteDst = isRemoteHost(dstHost);
    const isRemote = isRemoteSrc || isRemoteDst;
    const isAllRemote = isRemoteSrc && isRemoteDst;
    const cp = isRemote ? "scp" : "cp";

    const cmd = [cp, "-r"];
    if (isAllRemote) cmd.push("-3");
    cmd.push(srcFullPath);
    cmd.push(dstFullPath);

    function ret(): Promise<void>
    {
        const executor = makeExecCommand("", "", log);
        return executor(cmd);
    }
    return ret;
}


/**
 * @brief Check if the path exists..
 * @param {string} user The ssh user name.
 * @param {string} host The ssh host.
 * @param {MyLogger|null} log The logger.
 * @return {(path: string) => Promise<void>} The path-check function wrapper.
 */
function checkExists(
    user: string,
    host: string,
    log: MyLogger|null
): (path: string) => Promise<boolean>
{
    function checkResult(res: string): Promise<boolean>
    {
        return Promise.resolve(res.trim() === "true");
    }
    function ret(path: string): Promise<boolean>
    {
        const cmd = [
            // eslint-disable-next-line quotes
            `if [ -e ${path} ]; then echo "true"; else echo "false"; fi`
        ];
        log?.verboseLog("Checking existence with: " + cmd.join(" "));
        const executor = makeCollectStdoutCommand(user, host, log, true);
        return executor(cmd)
            .then(checkResult);
    }
    return ret;
}


export {
    checkExists,
    escapeCommand,
    isRemoteHost,
    makeCollectStdoutCommand,
    makeCopy,
    makeExecCommand,
    makePath,
    makeSpawnCommand
};
