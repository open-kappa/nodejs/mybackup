import {
    BackupCommand,
    JsonTarget,
    Smtp,
    Target,
    TargetResult,
    TaskResult,
    Telegram
} from "./mybackupImpl";


/**
 * @brief Manage the sending of reports.
 */
export class Audit
{
    private readonly command: BackupCommand;
    private readonly target: Target;
    private readonly config: JsonTarget;

    /**
     * @brief Constructor.
     * @param {BackupCommand} command The reference command.
     * @param {string} targetName The target name.
     * @param {JsonTarget} config The target config.
     */
    constructor(
        command: BackupCommand,
        target: Target,
        config: JsonTarget
    )
    {
        this.command = command;
        this.target = target;
        this.config = config;
    }

    /**
     * @brief Send the report.
     * @param {Array<TargetResult>} result The target result.
     * @return {Promise<void>} A completion promise.
     */
    run(results: Array<TargetResult>): Promise<void>
    {
        const self = this;
        const log = self.command.main.log;
        if (typeof self.config.audit === "undefined")
        {
            return Promise.resolve();
        }
        const audit = self.config.audit;
        log.log(self.makeLog("Managing reports..."));

        function findTargetWarnings(result: TargetResult): boolean
        {
            for (const res of result.results)
            {
                if (res === TaskResult.SUCCESS) continue;
                return true;
            }
            return result.gitResult !== TaskResult.SUCCESS
                || result.tgzResult !== TaskResult.SUCCESS;
        }
        function findTargetErrors(result: TargetResult): boolean
        {
            for (const res of result.results)
            {
                if (res !== TaskResult.ERROR) continue;
                return true;
            }
            return result.gitResult === TaskResult.ERROR
                || result.tgzResult === TaskResult.ERROR;
        }
        function findWarnings(): boolean
        {
            for (const res of results.values())
            {
                if (findTargetWarnings(res)) return true;
            }
            return false;
        }
        function findErrors(): boolean
        {
            for (const res of results.values())
            {
                if (findTargetErrors(res)) return true;
            }
            return false;
        }

        const hasWarnings = findWarnings();
        const hasErrors = findErrors();
        if (hasWarnings) self.command.setHasWarnings();
        if (hasErrors) self.command.setHasErrors();

        self.doLogMessage(results);
        let isTelegram = true;
        function onError(err: Error): Promise<void>
        {
            if (isTelegram)
            {
                log.error(
                    self.makeErrorString(
                        "Unable to send Telegram notification"
                    )
                );
            }
            else
            {
                log.error(
                    self.makeErrorString(
                        "Unable to send Email notification"
                    )
                );
            }
            log.error(err.message);
            return Promise.resolve();
        }
        function sendTelegram(): Promise<void>
        {
            isTelegram = true;
            if (audit.telegram.sendPolicy === "disabled"
                || (audit.telegram.sendPolicy === "warning"
                    && !hasWarnings)
                || (audit.telegram.sendPolicy === "error"
                    && !hasErrors)
            )
            {
                return Promise.resolve();
            }
            log.log(self.makeLog("Sending by Telegram..."));
            const telegram = new Telegram(
                audit.telegram.chatId,
                audit.telegram.botToken,
                audit.telegram.url
            );
            return telegram.sendMessage(self.getTelegramMessage(results))
                .catch(onError);
        }
        function sendEmail(): Promise<void>
        {
            isTelegram = false;
            if (audit.email.sendPolicy === "disabled"
                || (audit.email.sendPolicy === "warning"
                    && !hasWarnings)
                || (audit.email.sendPolicy === "error"
                    && !hasErrors)
            )
            {
                return Promise.resolve();
            }
            log.log(self.makeLog("Sending by email..."));
            const smtp = new Smtp(
                audit.email.host,
                audit.email.port,
                audit.email.secure,
                audit.email.user,
                audit.email.password,
                audit.email.from,
                audit.email.to
            );
            const emailObject = (typeof audit.email.objectTag !== "undefined"
                ? audit.email.objectTag + " "
                : "")
                + "Report backup " + self.target.getName()
                + " of " + self.target.getStartTimestamp().split(" ")[0];
            return smtp.sendEmail(
                self.getEmailMessage(results),
                emailObject,
                true
            ).catch(onError);
        }
        return sendTelegram()
            .then(sendEmail);
    }

    /**
     * @brief Get the telegram message.
     * @param {Array<TargetResult>} results The results.
     * @return {string} The message.
     */
    private getTelegramMessage(
        results: Array<TargetResult>
    ): string
    {
        const self = this;
        let message =
            Telegram.escape(BackupCommand.SEPARATOR) + "\n"
            + "*BACKUP RESULT*\n"
            + Telegram.escape(BackupCommand.SEPARATOR) + "\n";

        function getSingle(res: TargetResult): void
        {
            message
                += Telegram.escape(BackupCommand.SEPARATOR) + "\n"
                + "*TARGET " + Telegram.escape(res.targetName) + "*\n"
                + Telegram.escape(BackupCommand.SEPARATOR) + "\n";
            for (const [index, task] of res.names.entries())
            {
                const value = res.results[index];
                switch (value)
                {
                    case TaskResult.SUCCESS:
                        message += Telegram.escape(task) + ": SUCCESS\n";
                        break;
                    case TaskResult.WARNING:
                        message += Telegram.escape(task) + ": _WARNING_\n";
                        break;
                    case TaskResult.ERROR:
                        message += Telegram.escape(task) + ": *ERROR*\n";
                        break;
                    default:
                }
            }
            if (res.hasGit)
            {
                switch (res.gitResult)
                {
                    case TaskResult.SUCCESS:
                        message += "Git: SUCCESS\n";
                        break;
                    case TaskResult.WARNING:
                        message += "Git: _WARNING_\n";
                        break;
                    case TaskResult.ERROR:
                        message += "Git: *ERROR*\n";
                        break;
                    default:
                }
            }
            if (res.hasTgz)
            {
                switch (res.tgzResult)
                {
                    case TaskResult.SUCCESS:
                        message += "Tgz: SUCCESS\n";
                        break;
                    case TaskResult.WARNING:
                        message += "Tgz: _WARNING_\n";
                        break;
                    case TaskResult.ERROR:
                        message += "Tgz: *ERROR*\n";
                        break;
                    default:
                }
            }
        }

        for (const result of results.values())
        {
            getSingle(result);
        }
        message
            += Telegram.escape(BackupCommand.SEPARATOR) + "\n"
            + Telegram.escape(
                "Started at: " + self.target.getStartTimestamp() + "\n"
            )
            + Telegram.escape(
                "Completed at: " + self.target.getEndTimestamp() + "\n"
            )
            + Telegram.escape(BackupCommand.SEPARATOR) + "\n";

        return message;
    }

    /**
     * @brief Get the email message.
     * @param {Array<TargetResult>} results The results.
     * @return {string} The message.
     */
    private getEmailMessage(
        results: Array<TargetResult>
    ): string
    {
        const self = this;
        let message =
            BackupCommand.SEPARATOR + "<br/>"
            + "<b>BACKUP RESULT</b><br/>"
            + BackupCommand.SEPARATOR + "<br/>";

        function getSingle(res: TargetResult): void
        {
            message
                += BackupCommand.SEPARATOR + "<br/>"
                + "<b>TARGET " + res.targetName + "</b><br/>"
                + BackupCommand.SEPARATOR + "<br/>";
            for (const [index, task] of res.names.entries())
            {
                const value = res.results[index];
                switch (value)
                {
                    case TaskResult.SUCCESS:
                        message += task + ": SUCCESS<br/>";
                        break;
                    case TaskResult.WARNING:
                        message += task + ": <i>WARNING</i><br/>";
                        break;
                    case TaskResult.ERROR:
                        message += task + ": <b>ERROR</b><br/>";
                        break;
                    default:
                }
            }
            if (res.hasGit)
            {
                switch (res.gitResult)
                {
                    case TaskResult.SUCCESS:
                        message += "Git: SUCCESS<br/>";
                        break;
                    case TaskResult.WARNING:
                        message += "Git: <i>WARNING</i><br/>";
                        break;
                    case TaskResult.ERROR:
                        message += "Git: <b>ERROR</b><br/>";
                        break;
                    default:
                }
            }
            if (res.hasTgz)
            {
                switch (res.tgzResult)
                {
                    case TaskResult.SUCCESS:
                        message += "Tgz: SUCCESS<br/>";
                        break;
                    case TaskResult.WARNING:
                        message += "Tgz: <i>WARNING</i><br/>";
                        break;
                    case TaskResult.ERROR:
                        message += "Tgz: <b>ERROR</b><br/>";
                        break;
                    default:
                }
            }
        }

        for (const result of results.values())
        {
            getSingle(result);
        }
        message
            += BackupCommand.SEPARATOR + "<br/>"
            + "Started at: " + self.target.getStartTimestamp() + "<br/>"
            + "Completed at: " + self.target.getEndTimestamp() + "<br/>"
            + BackupCommand.SEPARATOR + "<br/>";

        return message;
    }

    /**
     * @brief Print the log message.
     * @param {Array<TargetResult>} results The results.
     */
    private doLogMessage(
        results: Array<TargetResult>
    ): void
    {
        const self = this;
        const log = self.command.main.log;
        log.info(BackupCommand.SEPARATOR);
        log.info("BACKUP RESULT");
        log.info(BackupCommand.SEPARATOR);

        function getSingle(res: TargetResult): void
        {
            log.info(BackupCommand.SEPARATOR);
            log.info("TARGET " + res.targetName);
            log.info(BackupCommand.SEPARATOR);
            for (const [index, task] of res.names.entries())
            {
                const value = res.results[index];
                switch (value)
                {
                    case TaskResult.SUCCESS:
                        log.log(task + ": SUCCESS");
                        break;
                    case TaskResult.WARNING:
                        log.warn(task + ": WARNING");
                        break;
                    case TaskResult.ERROR:
                        log.error(task + ": ERROR");
                        break;
                    default:
                }
            }
            if (res.hasGit)
            {
                switch (res.gitResult)
                {
                    case TaskResult.SUCCESS:
                        log.log("Git: SUCCESS");
                        break;
                    case TaskResult.WARNING:
                        log.warn("Git: WARNING");
                        break;
                    case TaskResult.ERROR:
                        log.error("Git: ERROR");
                        break;
                    default:
                }
            }
            if (res.hasTgz)
            {
                switch (res.tgzResult)
                {
                    case TaskResult.SUCCESS:
                        log.log("Tgz: SUCCESS");
                        break;
                    case TaskResult.WARNING:
                        log.warn("Tgz: WARNING");
                        break;
                    case TaskResult.ERROR:
                        log.error("Tgz: ERROR");
                        break;
                    default:
                }
            }
        }

        for (const result of results.values())
        {
            getSingle(result);
        }
        log.log(BackupCommand.SEPARATOR);
        log.log("Started at: " + self.target.getStartTimestamp());
        log.log("Completed at: " + self.target.getEndTimestamp());
        log.log(BackupCommand.SEPARATOR);
    }

    private makeError(msg: string): Error
    {
        const self = this;
        return new Error(self.makeErrorString(msg));
    }

    private makeErrorString(msg: string): string
    {
        const self = this;
        return self.target.getName() + "/audit: [ERROR] " + msg;
    }

    private makeWarn(msg: string): string
    {
        const self = this;
        return self.target.getName() + "/audit: [WARN] " + msg;
    }

    private makeLog(msg: string): string
    {
        const self = this;
        return self.target.getName() + "/audit: " + msg;
    }
}
