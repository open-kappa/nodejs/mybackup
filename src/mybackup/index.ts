#!/usr/bin/env node
import {
    Cli,
    Config,
    Main
} from "@open-kappa/myexe";
import {
    JsonConfig,
    SwitchCommand
} from "./mybackupImpl";
import {
    MyConsoleLogger,
    MyLogger
} from "@open-kappa/mylog";


/**
 * @brief The main entry point.
 */
function main(): void
{
    const mainInstance = new Main<JsonConfig>(
        MyLogger,
        Cli,
        Config,
        SwitchCommand,
        null
    );
    mainInstance.log.addLogger(new MyConsoleLogger());
    mainInstance.cli.addVerbose();
    mainInstance.cli.addConfig([], true, true);
    mainInstance.cli.addUnparsedArgs(2, "the command and its parameters");
    mainInstance.cli.addExtraComments(
        "",
        "COMMANDS",
        "",
        "backup <target>: execute the backup described by <target>"
    );
    mainInstance.enableLogging(true);
    mainInstance.run();
}

main();
