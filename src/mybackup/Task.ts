import {
    BackupCommand,
    checkExists,
    isRemoteHost,
    JsonTask,
    makeCopy,
    makeExecCommand,
    Target,
    TaskResult
} from "./mybackupImpl";
import {
    foreachPromise,
    toVoid
} from "@open-kappa/mypromise";
import path from "path";


export class Task
{
    name: string;
    target: Target;
    config: Array<JsonTask>;

    /**
     * @brief Constructor.
     * @param {string} name This task name.
     * @param {Target} target The target.
     * @param {Array<JsonTask>} config The task config list.
     */
    constructor(
        name: string,
        target: Target,
        config: Array<JsonTask>
    )
    {
        this.name = name;
        this.target = target;
        this.config = config;
    }

    /**
     * @brief Run this task.
     * @return {Promise<void>} A completion promise.
     */
    run(): Promise<TaskResult>
    {
        return this._runSingleTask(this.name);
    }

    /**
     * @brief Perform a single task.
     * @private
     * @param {string} task The task to execute.
     * @return {Promise<TaskResult>} The execution result.
     */
    private _runSingleTask(task: string): Promise<TaskResult>
    {
        const self = this;
        const log = self.target.command.main.log;
        log.log(BackupCommand.SEPARATOR);
        log.info("Task '" + self.target.getName() + "/" + task + "'");
        log.log(BackupCommand.SEPARATOR);

        function runSingleHost(taskObj: JsonTask): Promise<TaskResult>
        {
            return self._runSingleHost(task, taskObj);
        }
        function manageResult(res: Array<TaskResult>): Promise<TaskResult>
        {
            if (res.indexOf(TaskResult.ERROR) !== -1)
            {
                log.error(self.makeErrorString("Result: ERROR"));
                return Promise.resolve(TaskResult.ERROR);
            }
            else if (res.indexOf(TaskResult.WARNING) !== -1)
            {
                log.warn(self.makeWarn("Result: WARNING"));
                return Promise.resolve(TaskResult.WARNING);
            }
            log.log(self.makeLog("Result: SUCCESS"));
            return Promise.resolve(TaskResult.SUCCESS);
        }

        const backupHosts = foreachPromise(runSingleHost);
        return backupHosts(self.config)
            .then(manageResult);
    }

    /**
     * @brief Perform a single host task.
     * @private
     * @param {string} task The task to execute.
     * @param {JsonTask} taskObj The task object to execute.
     * @return {Promise<TaskResult>} The execution result.
     */
    private _runSingleHost(task: string, taskObj: JsonTask): Promise<TaskResult>
    {
        const self = this;
        self.target.command.main.log.log(
            self.makeLog(
                "Running for host/dir: "
                + (taskObj.host === ""
                    ? "localhost"
                    : taskObj.host
                )
                + " --> "
                + (taskObj.destinationSubdir === ""
                    ? "/"
                    : taskObj.destinationSubdir
                )
            )
        );
        if (taskObj.disabled)
        {
            self.target.command.main.log.log(self.makeLog("Host: disabled."));
            return Promise.resolve(TaskResult.SUCCESS);
        }

        let copyResult = TaskResult.SUCCESS;
        type CopyInfo = {
            host: string;
            path: string
            user: string;
        };
        function createDestinationDir(): Promise<void>
        {
            if (typeof taskObj.paths === "undefined"
                || taskObj.paths.length === 0)
            {
                return Promise.resolve();
            }
            const destination = self.target.destination;
            if (destination === null)
            {
                return Promise.reject(
                    new Error("Internal bug: destination is null")
                );
            }
            const execMake = makeExecCommand(
                destination.user,
                destination.host,
                self.target.command.main.log
            );
            let fullPath = path.join(destination.path, task);
            if (typeof taskObj.destinationSubdir !== "undefined")
            {
                fullPath = path.join(fullPath, taskObj.destinationSubdir);
            }
            const destinationCmd = [
                "mkdir",
                "-p",
                fullPath
            ];
            return execMake(destinationCmd);
        }
        function onCopyError(err: Error): Promise<void>
        {
            if (err.message === "")
            {
                copyResult = TaskResult.WARNING;
                return Promise.resolve();
            }
            return Promise.reject(err);
        }
        function onError(): Promise<void>
        {
            copyResult = TaskResult.ERROR;
            return Promise.resolve();
        }
        function executeSingleCommand(cmd: string): Promise<void>
        {
            const log = self.target.command.main.log;
            log.log("> " + cmd);
            const user = taskObj.user ?? "";
            const host = taskObj.host ?? "localhost";
            const executor = makeExecCommand(user, host, log);
            return executor([cmd]);
        }
        function execPreCommands(): Promise<void>
        {
            if (typeof taskObj.preCommands === "undefined"
                || taskObj.preCommands.length === 0)
            {
                return Promise.resolve();
            }
            const log = self.target.command.main.log;
            log.log(self.makeLog("Running pre-commands..."));
            const feach = foreachPromise(executeSingleCommand);
            return feach(taskObj.preCommands).then(toVoid);
        }
        function execSingleCopy(srcInfo: CopyInfo): Promise<void>
        {
            return self._execSingleCopy(
                path.join(task, taskObj.destinationSubdir ?? ""),
                srcInfo.user,
                srcInfo.host,
                srcInfo.path
            )
                .catch(onCopyError);
        }
        function execCopies(): Promise<void>
        {
            if (typeof taskObj.paths === "undefined"
                || taskObj.paths.length === 0)
            {
                return Promise.resolve();
            }
            const log = self.target.command.main.log;
            log.log(self.makeLog("Executing path copies..."));
            const copyCommands = new Array<CopyInfo>();
            const user = taskObj.user ?? "";
            const host = taskObj.host ?? "localhost";
            for (const taskPath of taskObj.paths.values())
            {
                copyCommands.push({
                    "host": host,
                    "path": taskPath,
                    "user": user
                });
            }
            const fe = foreachPromise(execSingleCopy);
            return fe(copyCommands)
                .then(toVoid);
        }
        function execPostCommands(): Promise<void>
        {
            if (typeof taskObj.postCommands === "undefined"
            || taskObj.postCommands.length === 0)
            {
                return Promise.resolve();
            }
            const log = self.target.command.main.log;
            log.log(self.makeLog("Running post-commands..."));
            const feach = foreachPromise(executeSingleCommand);
            return feach(taskObj.postCommands).then(toVoid);
        }
        function onCompletion(): Promise<TaskResult>
        {
            const log = self.target.command.main.log;
            switch (copyResult)
            {
                case TaskResult.SUCCESS:
                    log.log(self.makeLog("Host: SUCCESS"));
                    break;
                case TaskResult.WARNING:
                    log.warn(self.makeWarn("Host: WARNING"));
                    break;
                case TaskResult.ERROR:
                    log.error(self.makeErrorString("Host: ERROR"));
                    break;
                default:
            }
            return Promise.resolve(copyResult);
        }
        return createDestinationDir()
            .then(execPreCommands)
            .then(execCopies)
            .then(execPostCommands)
            .catch(onError)
            .then(onCompletion);
    }

    /**
     * @brief Copy from source to target recursively.
     * It can reject with a promise without message, to state that source path
     * does not exists (it must be considered as a warning).
     * @private
     * @param {string} dstSubfolder The destination subfolder.
     * @param {string} srcUser The ssh source user name.
     * @param {string} srcHost The ssh source host.
     * @param {string} srcPath The ssh source path.
     * @return {Promise<void>} A completion promise.
     */
    private _execSingleCopy(
        dstSubfolder: string,
        srcUser: string,
        srcHost: string,
        srcPath: string
    ): Promise<void>
    {
        const self = this;
        const log = self.target.command.main.log;
        function getPretty(
            user: string,
            host: string,
            thisPath: string
        ): string
        {
            if (!isRemoteHost(host)) return thisPath;
            return (user === "" ? "" : user + "@")
                + host + ":"
                + thisPath;
        }
        const dstObj = self.target.destination;
        if (dstObj === null)
        {
            return Promise.reject(
                new Error("Internal bug: destination is null")
            );
        }
        const dstUser = dstObj.user;
        const dstHost = dstObj.host;
        const dstPath = path.join(dstObj.path, dstSubfolder);
        const checkFoo = checkExists(srcUser, srcHost, log);
        function checkWarning(ok: boolean): Promise<void>
        {
            if (ok) return Promise.resolve();
            log.warn(
                self.makeWarn(
                    "Copy: "
                    + getPretty(srcUser, srcHost, srcPath)
                    + " does not exist."
                )
            );
            return Promise.reject(new Error());
        }
        function doCopy(): Promise<void>
        {
            log.log(
                self.makeLog(
                    "Copy: "
                    + getPretty(srcUser, srcHost, srcPath)
                    + " --> "
                    + getPretty(dstUser, dstHost, dstPath)
                )
            );
            const copyFoo = makeCopy(
                srcUser,
                srcHost,
                srcPath,
                dstUser,
                dstHost,
                dstPath,
                log
            );
            return copyFoo();
        }
        return checkFoo(srcPath)
            .then(checkWarning)
            .then(doCopy);
    }

    private makeError(msg: string): Error
    {
        const self = this;
        return new Error(self.makeErrorString(msg));
    }

    private makeErrorString(msg: string): string
    {
        const self = this;
        return self.target.getName() + "/" + self.name + ": [ERROR] " + msg;
    }

    private makeWarn(msg: string): string
    {
        const self = this;
        self.target.command.setHasWarnings();
        return self.target.getName() + "/" + self.name + ": [WARN] " + msg;
    }

    private makeLog(msg: string): string
    {
        const self = this;
        return self.target.getName() + "/" + self.name + ": " + msg;
    }
}
