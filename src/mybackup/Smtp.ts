import nodemailer, {SentMessageInfo} from "nodemailer";
import type Mail from "nodemailer/lib/mailer";


/**
 * @brief Minimal SMTP client.
 */
export class Smtp
{
    private readonly transporter: Mail;
    private readonly from: string;
    private readonly to: Array<string>;

    /**
     * @brief Constructor.
     * @param {string} host The SMTP server.
     * @param {number} port The server port.
     * @param {boolean} secure True if secure protocol must be used. Usually
     *     true for port 465.
     * @param {string} user The user nmae.
     * @param {string} password The password.
     * @param {string} from The sender.
     * @param {Array<string>} to The receivers list.
     */
    constructor(
        host: string,
        port: number,
        secure: boolean,
        user: string,
        password: string,
        from: string,
        to: Array<string>
    )
    {
        this.from = from;
        this.to = to;
        this.transporter = nodemailer.createTransport(
            {
                "auth": {
                    "pass": password,
                    "user": user
                },
                "host": host,
                "port": port,
                // usually: port === 465 --> secure === true
                "secure": secure
            }
        );
    }

    /**
     * @brief Send a message.
     * @param {string} message The message.
     * @param {string} subject The subject.
     * @param {boolean} isHtml True if the message is HTML formatted.
     * @return {Promise<void>} A completion promise.
     */
    sendEmail(
        message: string,
        subject: string,
        isHtml = false
    ): Promise<void>
    {
        const self = this;
        const opts: Mail.Options = {
            "html": isHtml ? message : "",
            "subject": subject,
            "text": isHtml ? "" : message,
            "to": self.to
        };
        if (self.from !== "") opts.from = self.from;

        function checkResult(info: SentMessageInfo): Promise<void>
        {
            const rejected: Array<any> = info.rejected;
            if (rejected.length !== 0)
            {
                return Promise.reject(new Error(JSON.stringify(rejected)));
            }
            return Promise.resolve();
        }
        return self.transporter.sendMail(opts)
            .then(checkResult);
    }
}
