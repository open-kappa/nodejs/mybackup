import {
    JsonConfig,
    JsonDestination,
    makeExecCommand
} from "./mybackupImpl";
import {DateTime} from "luxon";
import type {
    Main
} from "@open-kappa/myexe";

type RetentionInfo = {
    name: string;
    keep: boolean;
};

/**
 * A simple retention manager for TGZ backups.
 */
export class RetentionManager
{
    /**
     * @brief The reference to main.
     * @private
     */
    private readonly main: Main<JsonConfig>;

    /** The destination JSON config. */
    private readonly destination: JsonDestination;

    /** The backup dir. */
    private readonly backupDir: string;

    /**
     * @brief Constructor.
     * @param {Main<JsonConfig>} main The reference to main.
     * @param {string} target The command target.
     * @param {string} backupDir The backup dir.
     */
    constructor(
        main: Main<JsonConfig>,
        destination: JsonDestination,
        backupDir: string
    )
    {
        this.main = main;
        this.destination = destination;
        this.backupDir = backupDir;
    }

    /**
     * @brief Execute the policy.
     * @param {string} lsOut the output of the ls command.
     * @return {Promise<void>} A completion promise.
     */
    run(lsOut: string): Promise<void>
    {
        const self = this;

        function filterLs(value: string): boolean
        {
            return value.trim().endsWith(".tgz");
        }
        function trim(value: string): string
        {
            const parts = value.trim().split("/");
            const file = parts[parts.length - 1];
            return file.split(".")[0];
        }
        function init(value: string): RetentionInfo
        {
            return {
                "keep": false,
                "name": value
            };
        }

        const list = lsOut.split("\n")
            .filter(filterLs)
            .map(trim)
            .sort()
            .reverse()
            .map(init);

        self.retainDays(list);
        self.retainWeeks(list);
        self.retainMonths(list);
        self.retainYears(list);

        function getRemoveables(info: RetentionInfo): boolean
        {
            return !info.keep;
        }
        function toRmCommand(info: RetentionInfo): string
        {
            const cmd = "rm " + info.name + ".tgz";
            return cmd;
        }

        const rmList = list.filter(getRemoveables)
            .map(toRmCommand);

        return self._executeRm(rmList);
    }

    /**
     * @brief Retain the days.
     * @private
     * @param {Array<RetentionInfo>} list The list of backups.
     */
    private retainDays(list: Array<RetentionInfo>): void
    {
        const self = this;
        if (typeof self.destination.tgz === "undefined") return;
        const retention =
            self.destination.tgz.retention.days;
        self.basicRetain(list, retention, 3);
    }

    /**
     * @brief Retain the months.
     * @private
     * @param {Array<RetentionInfo>} list The list of backups.
     */
    private retainMonths(list: Array<RetentionInfo>): void
    {
        const self = this;
        if (typeof self.destination.tgz === "undefined") return;
        const retention =
            self.destination.tgz.retention.months;
        self.basicRetain(list, retention, 2);
    }

    /**
     * @brief Retain the years.
     * @private
     * @param {Array<RetentionInfo>} list The list of backups.
     */
    private retainYears(list: Array<RetentionInfo>): void
    {
        const self = this;
        if (typeof self.destination.tgz === "undefined") return;
        const retention =
            self.destination.tgz.retention.years;
        self.basicRetain(list, retention, 1);
    }

    /**
     * @brief Retain.
     * @private
     * @param {Array<RetentionInfo>} list The list of backups.
     * @param {number} retention The retention.
     * @param {number} many The kind: 1 === year, 2 === month, 3 === day.
     */
    private basicRetain(
        list: Array<RetentionInfo>,
        retention: number,
        many: number
    ): void
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        if (retention === 0) return;
        const max = retention === -1 ? list.length : retention;
        let prevSlot = "";
        let kept = 0;
        for (let i = 0; i < list.length; ++i)
        {
            const value = list[i];
            const parts = value.name.split("-");
            const thisSlot = parts[0]
                + (many >= 2 ? parts[1] : "")
                + (many >= 3 ? parts[2] : "");
            if (thisSlot === prevSlot) continue;
            prevSlot = thisSlot;
            if (i === 0 || kept >= max) continue;
            kept += 1;
            list[i - 1].keep = true;
        }
        if (kept < max && list.length !== 0) list[list.length - 1].keep = true;
    }

    /**
     * @brief Retain the weeks.
     * @private
     * @param {Array<RetentionInfo>} list The list of backups.
     */
    private retainWeeks(list: Array<RetentionInfo>): void
    {
        const self = this;
        if (typeof self.destination.tgz === "undefined") return;
        const retention =
        self.destination.tgz.retention.weeks;
        if (retention === 0) return;
        const max = retention === -1 ? list.length : retention;
        let prevWeek = -1;
        let kept = 0;
        for (let i = 0; i < list.length; ++i)
        {
            const value = list[i];
            const parts = value.name.split("-");
            const week = DateTime.local(
                parseInt(parts[0], 10),
                parseInt(parts[1], 10),
                parseInt(parts[2], 10)
            ).weekNumber;
            if (week === prevWeek) continue;
            prevWeek = week;
            if (i === 0 || kept >= max) continue;
            kept += 1;
            list[i - 1].keep = true;
        }
        if (kept < max && list.length !== 0) list[list.length - 1].keep = true;
    }

    /**
     * @brief Perform the actual remove.
     * @param {Array<string>} cmd The list of rm commands.
     * @return {Promise<void>} A completion promise.
     */
    private _executeRm(cmd: Array<string>): Promise<void>
    {
        const self = this;
        if (cmd.length === 0) return Promise.resolve();

        const cmdString = "cd " + self.backupDir + " && " + cmd.join(" && ");
        const cmdFoo = makeExecCommand(
            self.destination.user,
            self.destination.host,
            self.main.log
        );
        return cmdFoo([cmdString]);
    }
}
