import type {
    ICommand,
    Main
} from "@open-kappa/myexe";
import {
    JsonConfig,
    Target
} from "./mybackupImpl";


/**
 * @brief Implements a backup execution.
 */
export class BackupCommand
implements ICommand
{
    /** The separator for logging */
    static readonly SEPARATOR =
        "------------------------------------------------";

    /** The reference to main. */
    readonly main: Main<JsonConfig>;

    /** The command target. */
    private target: string;

    /** Whether some warnings occurred. */
    private hasWarnings: boolean;

    /** Whether some errors occurred. */
    private hasErrors: boolean;

    constructor(main: Main<JsonConfig>)
    {
        this.main = main;
        this.target = "";
        this.hasWarnings = false;
        this.hasErrors = false;
    }

    /**
     * @brief Set that a warning occurred.
     */
    setHasWarnings(): void
    {
        this.hasWarnings = true;
    }

    /**
     * @brief Set that an error occurred.
     */
    setHasErrors(): void
    {
        this.hasErrors = true;
    }

    /**
     * @brief The run method.
     * @return {Promise<void>} A completion promise.
     */
    run(): Promise<void>
    {
        const self = this;
        self.main.log.info("Executing the backup..");
        function configBackup(): Promise<void>
        {
            self.main.log.info("Configuring the backup...");
            return self.configBackup();
        }
        function runTarget(): Promise<void>
        {
            return self.runTarget();
        }


        function logCompletion(): Promise<void>
        {
            if (self.hasErrors)
            {
                return Promise.reject(
                    new Error("Some errors occurred.")
                );
            }
            else if (self.hasWarnings)
            {
                return Promise.reject(
                    new Error("Some warnings occurred.")
                );
            }
            self.main.log.info("Backup completed!");
            return Promise.resolve();
        }

        return configBackup()
            .then(runTarget)
            .then(logCompletion);
    }

    /**
     * @brief Configure the backup target.
     * @private
     * @return {Promise<void>} A completion promise.
     */
    private configBackup(): Promise<void>
    {
        const self = this;

        self.target = self.main.cli.getUnparsedArgs()[1];

        return Promise.resolve();
    }

    /**
     * @brief Run the top-level target.
     * @private
     * @return {Promise<void>} A completion promise.
     */
    private runTarget(): Promise<void>
    {
        const self = this;
        const tgt = new Target(self.target, self, null);
        return tgt.run(null);
    }
}
