/* eslint-disable no-shadow */

/**
 * @brief Possible results for tasks.
 */
export enum TaskResult {
    SUCCESS,
    WARNING,
    ERROR
}
