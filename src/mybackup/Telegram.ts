import {
    foreachPromise,
    toVoid
} from "@open-kappa/mypromise";
import https from "https";
import {IncomingMessage} from "http";


/**
 * @brief A minimal telegram client to send messages.
 */
export class Telegram
{
    static readonly TELEGRAM_DEFAULT_URL = "https://api.telegram.org";
    private readonly chatId: string;
    private readonly botToken: string;
    private readonly url: string;
    // Telegram sendMessage has a max of 4096 chars: let's reserve some margin:
    private static readonly MAX_LENGTH = 4000;


    /**
     * @brief Constructor.
     * @param {string} chatId The telegram chat ID.
     * @param {string} botToken The telegram bot token.
     * @param {string} url The telegram url.
     */
    constructor(
        chatId: string,
        botToken: string,
        url = Telegram.TELEGRAM_DEFAULT_URL
    )
    {
        this.chatId = chatId;
        this.botToken = botToken;
        this.url = url === "" ? Telegram.TELEGRAM_DEFAULT_URL : "";
    }

    /**
     * @brief Send a message.
     * @param {string} message The message.
     * @return {Promise<void>} A completion promise.
     */
    sendMessage(message: string): Promise<void>
    {
        const self = this;
        const url = self.url
            + "/bot" + this.botToken
            + "/sendMessage";

        const [hpart, path, action] = url.split("://")[1].split("/");
        const [host, port] = hpart.split(":");

        const headers = {
            "Content-Type": "application/json"
        };

        const params = {
            "headers": headers,
            "host": host,
            "method": "POST",
            "path": path ? `/${path}/${action}` : "/",
            "port": port || url.startsWith("https://") ? 443 : 80
        };

        let index = 0;
        const parts = self.getMessageParts(message);
        function singleSend(msg: string): Promise<void>
        {
            index += 1;
            return self.singleSend(
                msg,
                index,
                parts.length,
                params
            );
        }

        const sendAll = foreachPromise(singleSend);
        return sendAll(parts)
            .then(toVoid);
    }

    private getMessageParts(message: string): Array<string>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        const ret: Array<string> = [];
        const chunks = Math.floor(message.length / Telegram.MAX_LENGTH)
            + (message.length % Telegram.MAX_LENGTH === 0 ? 0 : 1);
        for (let i = 0; i < chunks; ++i)
        {
            const part = message.substring(
                i * Telegram.MAX_LENGTH,
                (i + 1) * Telegram.MAX_LENGTH
            );
            ret.push(part);
        }
        return ret;
    }

    private singleSend(
        msg: string,
        index: number,
        partsSize: number,
        params: any
    ): Promise<void>
    {
        const self = this;
        function send(): Promise<void>
        {
            return self.actualSingleSend(msg, index, partsSize, params);
        }
        return self.waitToSend().then(send);
    }

    private waitToSend(): Promise<void>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        function promiseImpl(
            resolve: () => void,
            _reject: (reason: Error) => void
        ): void
        {
            setTimeout(resolve, 1000);
        }
        const ret = new Promise<void>(promiseImpl);
        return ret;
    }

    private actualSingleSend(
        msg: string,
        index: number,
        partsSize: number,
        params: any
    ): Promise<void>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        const isMultipart = partsSize > 1;
        const postData = {
            "chat_id": self.chatId,
            "parse_mode": "MarkdownV2",
            "text": (isMultipart
                ? "(Part:" + index + "/" + partsSize + ")\n"
                : "")
                + msg
        };
        function promiseImpl(
            resolve: () => void,
            reject: (reason: Error) => void
        ): void
        {
            function onRequestCompletion(res: IncomingMessage): void
            {
                if (typeof res.statusCode === "undefined"
                    || res.statusCode < 200 || res.statusCode >= 300)
                {
                    reject(new Error(
                        "Error: " + res.statusMessage
                        + " Status Code: " + res.statusCode
                    ));
                    return;
                }
                resolve();
            }
            function onRequestError(err: Error): void
            {
                reject(err);
            }
            const req = https.request(params, onRequestCompletion);
            req.on("error", onRequestError);
            req.write(JSON.stringify(postData));
            req.end();
        }
        const ret = new Promise<void>(promiseImpl);
        return ret;
    }

    static escape(str: string): string
    {
        const escapeChar = "\\";
        const charsToEscape = [
            "_",
            "*",
            "[",
            "]",
            "(",
            ")",
            "~",
            "`",
            ">",
            "#",
            "+",
            "-",
            "=",
            "|",
            "{",
            "}",
            ".",
            "!"
        ];
        let ret = str;
        for (const char of charsToEscape)
        {
            ret = ret.split(char).join(escapeChar + char);
        }
        return ret;
    }
}
