import {
    JsonGit,
    JsonTgz
} from "../mybackupImpl";


/**
 * @brief The type of a target destination.
 */
export interface JsonDestination
{
    host: string;
    user: string;
    path: string;
    git?: JsonGit;
    tgz?: JsonTgz;
}
