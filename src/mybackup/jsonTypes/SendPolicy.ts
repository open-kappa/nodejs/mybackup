export type SendPolicy = "always" | "warning" | "error" | "disabled";
