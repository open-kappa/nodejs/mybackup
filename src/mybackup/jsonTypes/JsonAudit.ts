import {
    JsonEmail,
    JsonTelegram
} from "../mybackupImpl";


/**
 * @brief The audit setup.
 */
export interface JsonAudit
{

    /** Possible log file. */
    logFile: string;

    /**
     * List of logging scripts. They must take:
     * - The kind of log (log, info, warn, error, ...)
     * - The log message (escaped).
     */
    scripts: Array<string>;

    /** The email configuration */
    email: JsonEmail;

    /** The telegram configuration */
    telegram: JsonTelegram;
}
