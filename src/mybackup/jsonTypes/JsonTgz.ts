import {
    JsonRetention
} from "../mybackupImpl";


/**
 * @brief The type of a target destination directory.
 */
export interface JsonTgz
{
    enabled: boolean;
    retention: JsonRetention;
}
