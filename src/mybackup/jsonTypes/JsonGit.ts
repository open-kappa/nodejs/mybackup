/**
 * @brief The type of a target destination git repo.
 */
export interface JsonGit
{
    enabled: boolean;
    push: boolean;
}
