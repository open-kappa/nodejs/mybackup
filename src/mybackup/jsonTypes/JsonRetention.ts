/**
 * @brief The description of a retention policy.
 * Zero means no retention, -1 means always retain.
 */
export interface JsonRetention
{
    days: number;
    weeks: number;
    months: number;
    years: number;
}
