import {SendPolicy} from "../mybackupImpl";


/**
 * @brief The type of a report for telegram.
 */
export interface JsonTelegram
{
    sendPolicy: SendPolicy;
    chatId: string;
    botToken: string;
    url: string;
}
