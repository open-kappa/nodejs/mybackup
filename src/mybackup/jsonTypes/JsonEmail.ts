import {SendPolicy} from "../mybackupImpl";


/**
 * @brief The type of a email report.
 */
export interface JsonEmail
{
    sendPolicy: SendPolicy;
    host: string;
    port: number;
    secure: boolean;
    user: string;
    password: string;
    from : string;
    to: Array<string>;
    objectTag?: string;
}
