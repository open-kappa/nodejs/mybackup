export interface JsonTask
{
    destinationSubdir?: string;
    host?: string;
    user?: string;
    preCommands?: Array<string>;
    paths?: Array<string>;
    postCommands?: Array<string>;
    disabled?: boolean;
}
