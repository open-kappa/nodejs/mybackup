import {JsonTarget} from "../mybackupImpl";


/**
 * @brief Type for JSON configuration root.
 */
export type JsonConfig =
{

    /** The configuration file format version. */
    version: string;
} & {

    /** The set of available backup targets. */
    [targetName: string]: JsonTarget;
}
