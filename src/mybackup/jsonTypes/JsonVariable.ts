export interface JsonVariable
{
    name: string;
    value: string;
}
