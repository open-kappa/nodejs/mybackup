import {
    JsonAudit,
    JsonDestination,
    JsonTask
} from "../mybackupImpl";


/**
 * @brief A backup target type.
 */
export type JsonTarget =
{

    /** The description of the destination. */
    destination?: JsonDestination;

    /** The audit policies. */
    audit?: JsonAudit;

    /** Possible subtargets */
    subTargets?: Array<string>

    /** Whether this target is disabled */
    disabled?: boolean;
} & {

    /** The set of backup tasks. */
    [taskName: string]: Array<JsonTask>;
}
