import {
    TaskResult
} from "./mybackupImpl";


/**
 * @brief Possible results for tasks.
 */
export interface TargetResult {
    targetName: string;
    names: Array<string>;
    results: Array<TaskResult>;
    hasGit: boolean;
    hasTgz: boolean;
    gitResult: TaskResult;
    tgzResult: TaskResult;
}
