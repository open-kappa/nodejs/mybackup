import {
    BackupCommand,
    JsonConfig
} from "./mybackupImpl";
import type {
    ICommand,
    Main
} from "@open-kappa/myexe";


/**
 * Dispatch the command.
 */
export class SwitchCommand
implements ICommand
{
    /** The reference to main. */
    private readonly main: Main<JsonConfig>;

    /**
     * @brief Constructor.
     */
    constructor(main: Main<JsonConfig>)
    {
        this.main = main;
    }

    /**
     * @brief Run the command.
     * @return {Promise<void>} A completion promise.
     */
    run(): Promise<void>
    {
        const self = this;
        const unparsed = self.main.cli.getUnparsedArgs();
        switch (unparsed[0])
        {
            case "backup":
            {
                const cmd = new BackupCommand(self.main);
                return cmd.run();
            }
            default:
                return Promise.reject(
                    new Error("Unknown command: " + unparsed[0])
                );
        }
    }
}
