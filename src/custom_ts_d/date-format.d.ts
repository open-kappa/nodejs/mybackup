declare namespace asString {
    // eslint-disable-next-line no-shadow
    function asString(format?: Date | string, date?: Date): string;

    function parse(
        pattern: string,
        str: string,
        missingValuesDate?: Date
    ): Date;

    function now(): Date;

    const ISO8601_FORMAT = "yyyy-MM-ddThh:mm:ss.SSS";
    const ISO8601_WITH_TZ_OFFSET_FORMAT = "yyyy-MM-ddThh:mm:ss.SSSO";
    const DATETIME_FORMAT = "dd MM yyyy hh:mm:ss.SSS";
    const ABSOLUTETIME_FORMAT = "hh:mm:ss.SSS";
}
declare function asString(format?: Date | string, date?: Date): string;

export default asString;
