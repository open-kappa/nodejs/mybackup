#!/bin/bash

function get_script_dir()
{
    local DIR=""
    local SOURCE="${BASH_SOURCE[0]}"
    while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
        DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
        SOURCE="$(readlink "$SOURCE")"
        [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
    done
    THIS_SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
}
get_script_dir

function copy_d_ts()
{
    local NODE_NAME="$1"
    if [ -d src/custom_ts_d ]; then
        cp -r src/custom_ts_d dist
        if [ "$?" != "0" ]; then
            exit 1
        fi
    fi
}

cd $THIS_SCRIPT_DIR/..
copy_d_ts

# EOF
