
* Fork the project and perform the changes
* Ensure all original passing tests are still passing
* Add new tests to check your contributes
* Make a pull request
* Be ready to get feedbacks and perform some refinements before acceptance

## Copyright assignment

* We do not require any formal copyright assignment or contributor license
  agreement
* Any contributions intentionally sent upstream are presumed to be offered
  under the terms of the MIT license, with copyright assigned to MyPromise
  copyright holders
* The contributors names will be listed in the *contributors* section.
