The configuration is described in `.json` format.
Comments are supported.

```json
{
    // The configuration file format version.
    "version": "1.0.0",
    // The target name: there can be as many targets as desired.
    // This is the name to be passed as second argument on the command line.
    "backup-test": {
        // Where to store the backups:
        "destination": {
            // "localhost" or "": implies no ssh/scp.
            "host": "localhost",
            // user name for ssh/scp:
            "user": "",
            "path": "/home/test/backups",
            "git": {
                 // Whether the "destination.path" is a git repo:
                "enabled": false,
                // Whether do push of committed backups:
                "push": true
            },
            "tgz": {
                // Whether the "destination.path" is a plain directory:
                "enabled": true,
                // The retention of backups: zero means no retention, -1 means
                // always retain
                "retention": {
                    "days": 3,
                    "weeks": 3,
                    "months": 6,
                    "years": 3
                }
            }
        },
        // How to log and push alarms:
        "audit": {
            // The log file:
            "logFile": "/home/test/backups/log.txt",
            // Scripts to execute to perform additional logs. It is an array
            // of strings.
            "scripts": [],
            // Telegram configuration for alarms:
            "telegram": {
                // The send policy:
                // - "always": always send the backup report
                // - "warning": send if warnings or errors occurred
                // - "error": send if errors occurred
                // - "disabled": never send
                "sendPolicy": "warning",
                "chatId": "AAA-BBB",
                "botToken": "CCC_DDD",
                // Empty for the default telegram public url:
                "url": ""
            },
            // Email configuration for alarms:
            "email": {
                "sendPolicy": "warning",
                "host": "smtp.test.me",
                "port": 587,
                "secure": false,
                "user": "myself",
                "password": "super-secret",
                "from": "me@self.net",
                "to": ["me@self.net"]
            }
        },
        // Tasks are basic units of backups.
        // A set of tasks creates a target.
        // Each target will create a sub-directory inside the destination.path,
        // where the files will be created.
        "remote-task": [
            {
                // Possible sub-folder for destination copies.
                "destinationSubdir": "",
                // Please note that this is considered as a remote host
                // (use of ssh/scp enabled)
                "host": "127.0.0.1",
                "user": "kappa",
                // List of commands to run on the host before do copies:
                "preCommands": [],
                // List of files and directories to copy:
                "paths": [
                    "/home/kappa/.node-red/flows.json"
                ],
                // List of commands to run on the host after do copies:
                "postCommands": [],
                // Optional list of sub-target names to be run:
                "subTargets": [],
                // Whether to skip this block:
                "disabled": false
            }
        ],
        "local-task": [
            {
                "host": "localhost",
                "user": "",
                "preCommands": [],
                "paths": [
                    "/home/kappa/.node-red/flows.json"
                ],
                "postCommands": [
                    "echo \"Hello!\""
                ],
                "disabled": false
            }
        ],
        // Also targets can be disabled:
        "disabled": false
    }
}
```

Please note that for remote hosts, ssh keys must be set up properly,
since this program cannot perform a login with username/password.
