![MyBackup logo](mybackup.png)

This package provides a simple library to simplify the writing of simple
utility programs.

# Links

 * Project homepage: [hosted on GitLab Pages](
   https://open-kappa.gitlab.io/nodejs/mybackup)

 * Project sources: [hosted on gitlab.com](
   https://gitlab.com/open-kappa/nodejs/mybackup)

 * List of other OPEN-Kappa documentation: [hosted on GitLab Pages](
   https://open-kappa.gitlab.io)

# License

*@open-kappa/mybackup* is released under the liberal MIT License.
Please refer to the LICENSE.txt project file for further details.

# Patrons

This node module has been sponsored by [Gizero Energie s.r.l.](
www.gizeroenergie.it).
